from enum import Enum
import time
import asyncio

class Generator_Type(Enum):
    PSG = float("inf")
    UXG = 1
    VUXG = 2

class CommandServer:
    def __init__(self, add_command):
        self.add_command = add_command

    async def handle_echo(self, reader, writer):
        data = await reader.read(100)
        message = data.decode()
        addr = writer.get_extra_info('peername')

        #print(f"Received {message!r} from {addr!r}")

        delay, quad, command = message.split(",")

        self.add_command(int(delay), str(quad), str(command))

        #print(f"Send: {message!r}")
        writer.write(data)
        await writer.drain()

        #print("Close the connection")
        writer.close()

    async def start(self):
        print("We're up!")
        self.server = await asyncio.start_server(
            self.handle_echo, '127.0.0.1', 8888)

        addr = self.server.sockets[0].getsockname()
        print(f'Serving on {addr}')

        async with self.server:
                await self.server.serve_forever()

class Signal_Generator:
    def __init__(self, type, quadrant, ip, port):
        self.ip = ip
        self.port = port
        self.socket = print
        self.quadrant = ""

    async def sleep_execute(self, delay, command):
        print(f"IP: {self.ip}, Delay:{delay}, Command:{command}")
        return

class Generator_Manager():
    def __init__(self):
        self.command_server = CommandServer(self.add_command)
        self.running = False
        self.elapsed_time = time.perf_counter()
        self.start_time = time.perf_counter()
        
        self.soonest_release = {'1': float("inf"),
                                '2': float("inf"),
                                '3': float("inf"),
                                '4': float("inf"),
                                'lb_1': float("inf"),
                                'lb_2': float("inf"),
                                'lb_3': float("inf"),
                                'lb_4': float("inf")}

        # quad_to_gens_reference => {Quadrant: {ip:Generator} }
        self.quad_to_gens_reference = {'1': {},
                                       '2': {},
                                       '3': {},
                                       '4': {},
                                       'lb_1': {},
                                       'lb_2': {},
                                       'lb_3': {},
                                       'lb_4': {}}
        self.generator_busy_pool = {}

        # generator_free_pool => {Quadrant: {ip: Queue(Generator)} }
        self.generator_free_pool = {'1': asyncio.Queue(),
                                    '2': asyncio.Queue(),
                                    '3': asyncio.Queue(),
                                    '4': asyncio.Queue(),
                                    'lb_1': asyncio.Queue(),
                                    'lb_2': asyncio.Queue(),
                                    'lb_3': asyncio.Queue(),
                                    'lb_4': asyncio.Queue()}

        # command_queue => Pqueue [ (wait_time, (quad, command) ) ]
        self.command_queue = asyncio.PriorityQueue()

    async def add_command(self, wait_time, quadrant, command):
        if quadrant in self.quad_to_gens_reference and len(self.quad_to_gens_reference[quadrant]):
            elapsed_time = time.perf_counter()-self.start_time
            new_time = wait_time - elapsed_time
            print(wait_time, elapsed_time, self.start_time, new_time)
            await asyncio.sleep(new_time)
            item = (wait_time, (quadrant, command))
            self.command_queue.put_nowait(item)
            return True
        else:
            # No generators are assigned to this quadrant
            print (f"NO GENERATOR DEFINED FOR QUAD: {quadrant}")
            return False

    def add_commands(self, commands):
        for wait_time, quad, command in commands:
            self.add_command(wait_time, quad, command)
        return True

    def add_generator(self, device_type, quadrant, ip, port=666):
        new_generator = Signal_Generator(device_type, quadrant, ip, port)
        self.quad_to_gens_reference[quadrant][ip] = new_generator
        # print(self.quad_to_gens_reference)
        self.generator_free_pool[quadrant].put_nowait(
            self.quad_to_gens_reference[quadrant][ip])
        #print(self.generator_free_pool)
        return True

    def del_generator(self, device_type, quadrant, ip):
        # SG will no longer be queueable
        gen = self.quad_to_gens_reference[quadrant].get(ip)
        if gen:
            if gen.shutdown():
                self.quad_to_gens_reference[quadrant].delete(ip)
                return True
        else:
            return False

    async def cycle_start(self):
        server_task = asyncio.create_task(self.command_server.start())
        self.running = True
        self.start_time = time.perf_counter()
        #self.start_time = time.perf_counter_ns()
    
        while self.running:
            entry = await self.command_queue.get()
            delay, (quad, command) = entry
            task = asyncio.create_task(self.execute(quad, delay, command))
            self.command_queue.task_done()
        print("Downing server!")
        server_task.cancel()
    
    async def execute(self, quadrant, delay, command):
        def cleanup():
            self.generator_free_pool[quadrant].task_done()
            if self.soonest_release[quadrant] == delay: self.soonest_release[quadrant] = float("inf")
            self.generator_free_pool[quadrant].put_nowait(free_generator)

        def check_resources():
            if self.generator_free_pool[quadrant].empty():
                print("Pool " + str(quadrant))
                print(self.generator_free_pool[quadrant])
                print("ALERT! Resource starved: Generator Pool=> " + quadrant)
                print("Resource available in " + str(self.soonest_release[quadrant])+" seconds")

        check_resources()
        free_generator = await self.generator_free_pool[quadrant].get()
        self.soonest_release[quadrant] = min(self.soonest_release[quadrant], delay)
        await free_generator.sleep_execute(delay, command)
        cleanup()

    async def cycle_stop(self):
        print("Stopping")
        self.running = False
        #self.command_queue.queue.clear()