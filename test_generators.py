import Generators, pytest, asyncio
import NetGenerator

@pytest.fixture 
def event_loop(): 
    loop = asyncio.get_event_loop()
    #loop.run_forever() 
    yield loop 
    loop.close() 

@pytest.mark.asyncio
def test_generators(event_loop):
    gm = Generators.Generator_Manager()
    x = gm.add_generator(Generators.Generator_Type.UXG, '1', "192.168.1.1")
    gm.add_generator(Generators.Generator_Type.UXG, '1', "192.168.1.2")
    
    commands = [gm.add_command(2, '1',"echo1"),
        gm.add_command(3, '1',"echo2"),
        gm.add_command(5, '1',"echo3"),
        gm.add_command(7, '1',"echo4"),
        gm.add_command(12, '1',"echo4"),
        gm.add_command(19, '1',"echo4")]

    futures = [asyncio.ensure_future(i) for i in commands]
    futures.append(event_loop.create_task(gm.cycle_start()))
    futures.append(asyncio.ensure_future(scoring_tool(gm)))
    # DON'T FORGET ASYNC SCORING CREATE TASK!
    event_loop.run_until_complete(asyncio.wait(futures))
    # End of the line
    #To get past here, the scoring tool must call gm.cycle_stop()
    print("DONE!")
    assert True

async def scoring_tool(gm):
    print("Sleeping lol...")
    await asyncio.sleep(10)
    await gm.cycle_stop()
